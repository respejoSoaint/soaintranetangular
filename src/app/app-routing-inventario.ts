import { Routes, RouterModule } from '@angular/router';
import { TipoInventarioComponent } from './components/inventario/administracion/tipo-inventario/tipo-inventario.component';
import { CrearInventarioComponent } from './components/inventario/gestion/crear-inventario/crear-inventario.component';


export const INVENTARIO_ROUTES: Routes = [
    { path: 'crear', component: CrearInventarioComponent },
    { path: 'administracion/tipoinventarios/crear', component: TipoInventarioComponent },
    { path: '**' , redirectTo: '404' }
];
