import { Routes, RouterModule } from '@angular/router';
import { CrearDocumentoComponent } from './components/documentos/gestion/crear-documento/crear-documento.component';
import { TipoDocumentoComponent } from './components/documentos/administracion/tipo-documento/tipo-documento.component';


export const DOCUMENTOS_ROUTES: Routes = [
    { path: 'crear', component: CrearDocumentoComponent },
    { path: 'administracion/tipodocumento/crear' , component: TipoDocumentoComponent },
    { path: '**' , redirectTo: '404' }
];

