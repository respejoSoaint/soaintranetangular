import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http'; 
import { AppRoutingModule } from './app-routing.module';

//import { AlertModule } from 'ngx-bootstrap';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/intranet/dashboard/dashboard.component';
import { RegistroComponent } from './components/registro/registro.component';
import { ReciboPagoComponent } from './components/nomina/recibo-pago/recibo-pago.component';
import { PreNominaComponent } from './components/nomina/pre-nomina/pre-nomina.component';
import { MenuComponent } from './components/menu/menu.component';
import { Error404Component } from './components/error404/error404.component';
import { HomeComponent } from './components/home/home.component';
import { UsuariosComponent } from './components/administracion/usuarios/usuarios.component';
import { LoadingComponent } from './components/loading/loading.component';
import { VisualizarUsuarioComponent } from './components/administracion/visualizar-usuario/visualizar-usuario.component';

//RECLUTAMIENTO
import { CreateComponent } from './components/reclutamiento/candidato/create/create.component';
import { VisualizarCandidatoComponent } from './components/reclutamiento/candidato/visualizar-candidato/visualizar-candidato.component';
import { SolicitudComponent } from './components/reclutamiento/solicitud/solicitud.component';
import { CandidatoComponent } from './components/reclutamiento/candidato/candidato.component';
import { AspiranteComponent } from './components/aspirante/aspirante.component';
import { DatosLaboralesComponent } from './components/aspirante/datos-laborales/datos-laborales.component';
import { DatosAcademicosComponent } from './components/aspirante/datos-academicos/datos-academicos.component';
import { HabilidadesComponent } from './components/aspirante/habilidades/habilidades.component';
import { ArchivosComponent } from './components/aspirante/archivos/archivos.component';
import { ReclutamientoComponent } from './components/reclutamiento/reclutamiento/reclutamiento.component';
import { CrearComponent } from './components/reclutamiento/solicitud/crear/crear.component';
import { VisualizarSolicitudComponent } from './components/reclutamiento/solicitud/visualizar-solicitud/visualizar-solicitud.component';
import { EmpleadosComponent } from './components/reclutamiento/empleados/empleados.component';
import { ObservacionContratoComponent } from './components/reclutamiento/empleados/observacion-contrato/observacion-contrato.component';
import { RecursosInternosComponent } from './components/reclutamiento/empleados/recursos-internos/recursos-internos.component';
import { VisualizarRecursoInternoComponent } from './components/reclutamiento/empleados/recursos-internos/visualizar-recurso-interno/visualizar-recurso-interno.component';

//INTRANET

import { ControlAccesoComponent } from './components/intranet/control-acceso/control-acceso.component';
import { ControlActividadesComponent } from './components/intranet/control-actividades/control-actividades.component';
import { RelojComponent } from './components/intranet/control-acceso/reloj/reloj.component';
import { PublicacionesComponent } from './components/home/publicaciones/publicaciones.component';
import { NavbarComponent } from './components/home/navbar/navbar.component';
import { FooterComponent } from './components/home/footer/footer.component';
import { InfoIntranetComponent } from './components/home/info-intranet/info-intranet.component';
import { CelebracionesComponent } from './components/home/celebraciones/celebraciones.component';
import { MuroComponent } from './components/intranet/dashboard/muro/muro.component';
import { ActividadesComponent } from './components/intranet/dashboard/actividades/actividades.component';
import { NewsComponent } from './components/intranet/dashboard/news/news.component';
import { AuditoriaAccesoComponent } from './components/intranet/auditoria-acceso/auditoria-acceso.component';
import { VerticaltabsComponent } from './components/intranet/auditoria-acceso/verticaltabs/verticaltabs.component';
import { PersonalactivoComponent } from './components/intranet/auditoria-acceso/verticaltabs/personalactivo/personalactivo.component';
import { PersonalinactivoComponent } from './components/intranet/auditoria-acceso/verticaltabs/personalinactivo/personalinactivo.component';
import { VarActividadesComponent } from './components/intranet/dashboard/actividades/var-actividades/var-actividades.component';
import { TodayactivityComponent } from './components/intranet/dashboard/actividades/todayactivity/todayactivity.component';
import { PersonalComponent } from './components/intranet/auditoria-acceso/personal/personal.component';
import { ColectivoComponent } from './components/intranet/auditoria-acceso/colectivo/colectivo.component';
import { ReportePersonalComponent } from './components/intranet/auditoria-acceso/personal/reporte-personal/reporte-personal.component';
import { TodayactivitysComponent } from './components/intranet/control-actividades/todayactivitys/todayactivitys.component';
import { HistorialActividadesComponent } from './components/intranet/control-actividades/historial-actividades/historial-actividades.component';
import { AuditoriaActividadesComponent } from './components/intranet/auditoria-actividades/auditoria-actividades.component';
import { ReporteactividadesComponent } from './components/intranet/auditoria-actividades/reporteactividades/reporteactividades.component';
import { NoticiasComponent } from './components/intranet/dashboard/noticias/noticias.component';
import { NextEventComponent } from './components/intranet/dashboard/next-event/next-event.component';
import { PoliticasAsistenciaComponent } from './components/intranet/control-acceso/politicas/politicas-asistencia/politicas-asistencia.component';
import { IniciosoaComponent } from './components/intranet/iniciosoa/iniciosoa.component';
//PROYECTOS
import { CrearAsignacionComponent } from './components/proyectos/asignaciones/crear-asignacion/crear-asignacion.component';
import { CrearTareaComponent } from './components/proyectos/tareas/crear-tarea/crear-tarea.component';
import { CrearProyectoComponent } from './components/proyectos/proyecto/crear-proyecto/crear-proyecto.component';
//DOCUMENTOS
import { CrearDocumentoComponent } from './components/documentos/gestion/crear-documento/crear-documento.component';
import { TipoDocumentoComponent } from './components/documentos/administracion/tipo-documento/tipo-documento.component';
//INVENTARIOS
import { CrearInventarioComponent } from './components/inventario/gestion/crear-inventario/crear-inventario.component';
import { TipoInventarioComponent } from './components/inventario/administracion/tipo-inventario/tipo-inventario.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    RegistroComponent,
    ReciboPagoComponent,
    PreNominaComponent,
    ControlAccesoComponent,
    ControlActividadesComponent,
    MenuComponent,
    Error404Component,
    HomeComponent,
    UsuariosComponent,
    LoadingComponent,
    //RECLUTAMIENTO
    CreateComponent,
    SolicitudComponent,
    CandidatoComponent,
    VisualizarCandidatoComponent,
    AspiranteComponent,
    DatosLaboralesComponent,
    DatosAcademicosComponent,
    HabilidadesComponent,
    ArchivosComponent,
    ReclutamientoComponent,
    VisualizarUsuarioComponent,
    CrearComponent,
    VisualizarSolicitudComponent,
    EmpleadosComponent,
    ObservacionContratoComponent,
    RecursosInternosComponent,
    VisualizarRecursoInternoComponent,
    //INTRANET
    RelojComponent,
    PublicacionesComponent,
    NavbarComponent,
    FooterComponent,
    InfoIntranetComponent,
    CelebracionesComponent,
    MuroComponent,
    ActividadesComponent,
    NewsComponent,
    AuditoriaAccesoComponent,
    VerticaltabsComponent,
    PersonalactivoComponent,
    PersonalinactivoComponent,
    VarActividadesComponent,
    TodayactivityComponent,
    PersonalComponent,
    ColectivoComponent,
    ReportePersonalComponent,
    TodayactivitysComponent,
    HistorialActividadesComponent,
    AuditoriaActividadesComponent,
    ReporteactividadesComponent,
    NoticiasComponent,
    NextEventComponent,
    PoliticasAsistenciaComponent,
    IniciosoaComponent,
    //PROYECTOS
    CrearAsignacionComponent,
    CrearTareaComponent,
    CrearProyectoComponent,
    //DOCUMENTOS
    CrearDocumentoComponent,
    TipoDocumentoComponent,
    //INVENTARIOS
    CrearInventarioComponent,
    TipoInventarioComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
