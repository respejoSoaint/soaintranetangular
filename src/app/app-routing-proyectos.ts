import { Routes, RouterModule } from '@angular/router';
import { CrearAsignacionComponent } from './components/proyectos/asignaciones/crear-asignacion/crear-asignacion.component';
import { CrearTareaComponent } from './components/proyectos/tareas/crear-tarea/crear-tarea.component';
import { CrearProyectoComponent } from './components/proyectos/proyecto/crear-proyecto/crear-proyecto.component';


export const PROYECTOS_ROUTES: Routes = [
    { path: 'tareas/crear', component: CrearTareaComponent },
    { path: 'proyecto/crear' , component: CrearProyectoComponent },
    { path: 'asignar/actividades', component: CrearAsignacionComponent}, 

    { path: '**' , redirectTo: '404' }
];

