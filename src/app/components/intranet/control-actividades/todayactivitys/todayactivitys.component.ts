import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { Registroactividades } from '../../../../models/intranet/registroactividades';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { UsersService } from '../../../../services/users.service';
import  { ControlActividadesService }  from '../../../../services/intranet/control-actividades.service';

@Component({
  selector: 'app-todayactivitys',
  templateUrl: './todayactivitys.component.html',
  styleUrls: ['./todayactivitys.component.css']
})
export class TodayactivitysComponent implements OnInit {
	public RegistroactividadesModel = new Registroactividades(null, null, null, null, null);
	public identity;
  public status: String;
  public message: String;
  public mes;
  public dia;
  public min;
  public fecha: String;
  public messagetwo: String;
  constructor(private _userService : UsersService, private _ControlActividadesService: ControlActividadesService,) {
  	this.identity = this._userService.getIdentity();
   }

  ngOnInit() {
    let f = new Date();
    this.mes = f.getMonth()+1;
    this.min = f.getMonth();
    this.dia = f.getDate();

    if(this.dia<10){
      this.dia = '0'+this.dia;
    }

    if(this.mes<10){
      this.mes = '0'+this.mes;
    }

    this.fecha = f.getFullYear() + "-" + this.mes + "-" + this.dia;
    this.message = this.fecha;
    this.min = f.getFullYear() + "-" + this.mes + "-" + '01';
    this.messagetwo = this.min;
    console.log(this.min);
  }

	registroActividad(form){
  	this.RegistroactividadesModel.id_user = this.identity.sub;
  	console.log(this.RegistroactividadesModel);
    this._ControlActividadesService.registroActividades(this.RegistroactividadesModel).subscribe(
        response => {
        console.log(response);
        if(response.status == 'success'){
          this.status = response.status;
          this.message = response.message;
          this.ngOnInit();
          
         }else{
           this.status = response.status;
           this.message = response.message;
         }
        }
      );
	}
}
